<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

Route::any('allnews','api/Index/indexInfo');#首页展示所有新闻   get/post

Route::any('cates','api/Cate/cates');#所有分类                  get/post
Route::any('adnews','api/Cate/index');#分类展示页展示该分类所有新闻及广告  post请求 需要cate_id

Route::any('register','api/Login/register');#注册     post请求 需要user_name,user_pwd
Route::any('login','api/Login/login');#登录           post请求 需要user_name,user_pwd
Route::any('logout','api/Login/logout');#退出         get/post
Route::any('newsinfo','api/News/index');#新闻详情页   post请求 需要news_id

//Route::any('usernews','api/Usernews/index');#该用户所有的收藏
//Route::any('newsdel','api/Usernews/usernewsdel');#删除该用户的某个收藏
Route::any('addnews','api/Usernews/adduser_news');#添加收藏         post请求 需要news_id
Route::any('addcomment','api/Comment/addcomment');#添加评论     post请求 需要news_id,comment_content