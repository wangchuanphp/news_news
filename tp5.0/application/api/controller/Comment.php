<?php

namespace app\api\controller;

use app\api\model\comments;
use think\Controller;
use think\Db;
use think\Request;
use think\Session;

class Comment extends Controller
{
    #添加评论
    public function addcomment()
    {
        header('Access-Control-Allow-Origin:*');
        //先判断是否登录
        $token_content=request()->post('token');
        $token_id=request()->post('token_id');
        $data=json_decode(request()->post('data'));
        $token=Db::table('token')->where('token_id',$token_id)->find();
//        var_dump($data);exit;
        if($token['token_content']==$token_content){
            $news_id=request()->post('news_id');
            $content=input('post.comment_content');
            if($time=comments::addcomment($data->user_id,$content,$news_id)){
                return json(['status'=>1,'msg'=>'评论成功','time'=>$time]);
            }else{
                return json(['status'=>0,'msg'=>'评论失败']);
            }
        }else{
            return show(2,"未登录");
        }
    }

}
