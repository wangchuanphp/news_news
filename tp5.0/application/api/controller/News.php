<?php
namespace app\api\controller;

use think\Controller;
use think\Validate;
use think\Request;
use think\Session;
use think\Db;

class News extends Controller{
    public function index(){

        if(request()->isPost()){
            $news_id=request()->post('news_id');
            header('Access-Control-Allow-Origin:*');
            //点击新闻展示新闻，包括新闻的评论
            $news=Db::table('news')->where('news_id',$news_id)->find();
            $status=Db::table('user_news')->where('news_id',$news_id)->where('user_id',Session('user')['user_id'])->find();
            if($status){
                $news['status']=1;
            }else{
                $news['status']=0;
            }
            $comment=Db::table('comment')->where('news_id',$news_id)->select();
            foreach($comment as $key => $val){
                $user=Db::table('user')->field('user_name,user_face')->where('user_id',$val['user_id'])->find();
                $comment[$key]=array_merge($val,$user);
                $comment[$key]['comment_time']=date("Y-m-d H:i:s",$val['comment_time']);
            }
            $img=Db::table('img')->where('news_id',$news_id)->find();
            if($news && $comment){
                return json(['status'=>1,'data'=>$news,'datas'=>$comment,'img'=>$img]);
            }else if($news && !$comment){
                return json(['status'=>1,'data'=>$news,'img'=>$img]);
            }
        }
    }
}