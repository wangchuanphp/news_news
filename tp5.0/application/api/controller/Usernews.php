<?php

namespace app\api\controller;

use app\api\model\usernewss;
use think\Controller;
use think\Cookie;
use think\Db;
use think\Request;
use think\Session;

class Usernews extends Controller
{
    #该用户所有收藏的展示
    public function index()
    {
        header('Access-Control-Allow-Origin:*');
        $news_id=request()->post('news_id');
        $data=usernewss::user_newsall();
        if($data){
            return show(1,"查到数据",$data);
        }else{
            return show(0,"该用户没有任何收藏");
        }
    }

    #删除该用户的收藏
    public function usernewsdel()
    {
        header('Access-Control-Allow-Origin:*');
        $news_id=request()->post('news_id');
        $news_id=input('post.news_id');
        if(usernewss::user_newsdel($news_id)){
            return show(1,"删除成功");
        }else{
            return show(1,"删除成功");
        }
    }


    #添加收藏
    public static function adduser_news(){
        //判断是否登录
        header('Access-Control-Allow-Origin:*');
        $token_content=request()->post('token');
        $token_id=request()->post('token_id');
        $data=json_decode(request()->post('data'));
        $token=Db::table('token')->where('token_id',$token_id)->find();
        if($token['token_content']==$token_content){
            $news_id=request()->post('news_id');
            if(usernewss::addnews($data->user_id,$news_id)){
                return show(1,"添加收藏成功");
            }else{
                return show(0,"添加收藏失败");
            }
        }else{
            return show(2,"未登录");
        }

    }

}
