<?php

namespace app\api\controller;

use think\Controller;
use think\Cookie;
use think\Validate;
use think\Request;
use think\Session;
use think\Db;

class Login extends Controller{
    //前台用户注册
    public function register(){
        if(request()->isPost()){
            header('Access-Control-Allow-Origin:*');
            $user_name=request()->post('user_name');
            $user_pwd=request()->post('user_pwd');
            $data=[
                'user_name'=>$user_name,
                'user_pwd' =>$user_pwd
            ];
            $user_id=Db::table('user')->insertGetId($data);
            if($user_id){
                //新用户注册后直接跳转到展示页则把以下注释打开
                $user=Db::table('user')
                    ->where('user_id',$user_id)
                    ->find();
                $token=md5($user_name.$user_pwd);
                Db::table('token')->insert(['token_content' => $token]);
                $token_id=Db::table('token')->getLastInsID();
                Session::set('token',$token);
                return json(['status'=>1,'msg'=>'注册成功','data'=>$user,'token_id'=>$token_id,'token'=>$token]);
            }else{
                return json(['status'=>0,'msg'=>'注册失败']);
            }
        }
    }

    //前台用户登录
    public function login(){
//        if(request()->isGet()){
//            $token=md5("111111111111111111111");
//            Session::set('oken',$token);
//        }
        if(request()->isPost()){
            header('Access-Control-Allow-Origin:*');
            $user_name=request()->post('user_name');
            $user_pwd=request()->post('user_pwd');
            //验证用户名及密码
            //查询
            $user=Db::table('user')
                ->where('user_name',$user_name)
                ->where('user_pwd',$user_pwd)
                ->find();
            if($user){
                //验证成功,存session
                Session::set('user',$user);
                $token=md5($user_name.$user_pwd);
                Db::table('token')->insert(['token_content' => $token]);
                $token_id=Db::table('token')->getLastInsID();
                return json(['status'=>1,'msg'=>'登录成功','data'=>$user,'token'=>$token,'token_id'=>$token_id]);
            }else{
                //验证失败
                return json(['status'=>0,'msg'=>'登录失败']);
            }
        }
    }

    //前台用户退出
    public function logout(){
        header('Access-Control-Allow-Origin:*');
            $token_id=request()->post('token_id');
            Db::name("token")->where('token_id',$token_id)->delete();
            return json(['status'=>1,'msg'=>'退出成功']);

    }
}
