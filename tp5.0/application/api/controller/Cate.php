<?php
namespace app\api\controller;
use think\Controller;
use think\Request;
use think\Db;

class Cate extends controller
{
    public function index()
    {
        if(request()->isPost()){
            header('Access-Control-Allow-Origin:*');
            $cate_id=request()->post('cate_id');
            $ncate=Db::table('news')->where('cate_id',$cate_id)->select();
            foreach($ncate as $key => $val){
                $img=Db::table('img')->field('img_url')->where('news_id',$val['news_id'])->select();
                foreach($img as $k =>$v){
                    $ncate[$key]=array_merge($val,$v);
                }
            }
            $ads=Db::table('ad')->select();
            return json(['ncate'=>$ncate,'ads'=>$ads]);
        }

    }
    public function cates()
    {header('Access-Control-Allow-Origin:*');
        $cates=Db::table('cate')->select();
        return json(['cates'=>$cates]);
    }

}
