<?php

namespace app\api\controller;

use think\Controller;
use think\Db;
use think\Request;

class Index extends Controller{
    //首页展示信息接口
    public function indexInfo(){
        header('Access-Control-Allow-Origin:*');
        $index=new \app\api\model\index();
        $newsInfo=$index->newsInfo();
        $newsTime=$index->newsTime();
        $news=$index->groupNews($newsInfo,$newsTime);
        return json(['status'=>1,'msg'=>"查询成功",'data'=>$news]);
    }
}
