<?php

namespace app\api\model;

use think\Model;
use think\Session;
class usernewss extends Model
{
    protected $table = 'user_news';
    #查询该用户的所有收藏
    public static function user_newsall(){
//        $data=['user_id'=>2,'user_name'=>"张三"];
//         session::set('data',$data);
          $user=session('user');
        return self::where('user_id',$user['user_id'])->join('news','news.news_id=user_news.news_id')->select()->toArray();
    }


    #删除该用户的收藏
    public static function user_newsdel($news_id){
        $user=session('user');
        return self::where('news_id',$news_id)->where('user_id',$user['user_id'])->delete();
    }

    #添加收藏
    public static function addnews($user_id,$news_id){
        return self::create(['user_id'=>$user_id,'news_id'=>$news_id]);
    }
}
