<?php

namespace app\api\model;

use think\Model;

class comments extends Model
{
    protected $table="comment";

    #添加评论
    public static function addcomment($user_id,$content,$news_id){
      self::create(['comment_content'=>$content,'comment_time'=>time(),'user_id'=>$user_id,'news_id'=>$news_id]);
      return date('Y-m-d H:i:s',time());
    }
}
