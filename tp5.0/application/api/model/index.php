<?php

namespace app\api\model;

use Think\Db;
use think\Model;

class index extends Model{
    protected $table = 'news';
    //查询所有的新闻
    public function newsInfo(){
        $newsInfo=Db::table('news')->limit(8)->select();
        foreach($newsInfo as $key => $val){
            $imgs=Db::table('img')->field('img_url')->where('news_id',$val['news_id'])->select();
            foreach($imgs as $k => $v){
                $newsInfo[$key]=array_merge($val,$v);
            }
        }
        return $newsInfo;
    }
    //查询时段的新闻
    public function newsTime(){
        $time=time();
        $todayBegin=$time-24*3600;
        $today=Db::table('news')->where('news_create_time','>',$todayBegin)->limit(4)->select();//今日新闻
        $yesterdayBegin=$time-48*3600;
        $yesterday=Db::table('news')->where('news_create_time','<',$todayBegin)->where('news_create_time','>',$yesterdayBegin)->limit(4)->select();//昨日新闻
        $lastdayBegin=$time-72*3600;
        $lastday=Db::table('news')->where('news_create_time','<',$yesterdayBegin)->where('news_create_time','>',$lastdayBegin)->limit(4)->select();//前天新闻
        $threeDay=[];
        foreach($today as $key => $val){
            $imgs=Db::table('img')->field('img_url')->where('news_id',$val['news_id'])->select();
            foreach($imgs as $k => $v){
                $val=array_merge($val,$v);
            }
            $threeDay['today'][]=$val;
        }
        foreach($yesterday as $key => $val){
            $imgs=Db::table('img')->field('img_url')->where('news_id',$val['news_id'])->select();
            foreach($imgs as $k => $v){
                $val=array_merge($val,$v);
            }
            $threeDay['yesterday'][]=$val;
        }
        foreach($lastday as $key => $val){
            $imgs=Db::table('img')->field('img_url')->where('news_id',$val['news_id'])->select();
            foreach($imgs as $k => $v){
                $val=array_merge($val,$v);
            }
            $threeDay['lastday'][]=$val;
        }
        return $threeDay;
    }
    //首页总接口数据整合
    public function groupNews($newsInfo,$threeDay){
        $news=["allNews"=>$newsInfo,"threeDays"=>$threeDay];
        return $news;
    }

}
