<?php
namespace app\admin\model;
use think\Model;
use Qiniu\Auth;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;

class ads extends Model
{
    protected $table = 'ad';
    //广告的查
    public static function adall (){
        return self::all()->toArray();
    }
    //公共的
    public static function updatefile($file){
        {
// 要上传图片的本地路径
            $filePath = $file->getRealPath();
            $ext = pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);  //后缀
//获取当前控制器名称
            $controllerName = 'index';
// 上传到七牛后保存的文件名
            $key =substr(md5($file->getRealPath()) , 0, 5). date('YmdHis') . rand(0, 9999) . '.' . $ext;
// 需要填写你的 Access Key 和 Secret Key
            $accessKey = config('ACCESSKEY');
            $secretKey = config('SECRETKEY');
// 构建鉴权对象
            $auth = new Auth($accessKey, $secretKey);
// 要上传的空间
            $bucket = config('BUCKET');
            $domain = config('DOMAINImage');
            $token = $auth->uploadToken($bucket);
// 初始化 UploadManager 对象并进行文件的上传
            $uploadMgr = new UploadManager();
// 调用 UploadManager 的 putFile 方法进行文件的上传
            list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);
            if ($err !== null) {
                echo ["err"=>1,"msg"=>$err,"data"=>""];
                return false;
            } else {
                //返回图片的完整URL
                $url = "http://p94w98kxt.bkt.clouddn.com/".$ret['key'];
                return $url;
            }
        }
    }

//广告的添加
    public static function adadd($file){
        if(empty($file)){
            return self::create(['ad_start_time'=>strtotime(input('post.ad_start_time')),'ad_end_time'=>strtotime(input('post.ad_end_time')),'ad_status'=>input('post.ad_status')]);
        }else{
            $url=self::updatefile($file);
            if($url){
                return self::create(['ad_img'=>$url,'ad_start_time'=>strtotime(input('post.ad_start_time')),'ad_end_time'=>strtotime(input('post.ad_end_time')),'ad_status'=>input('post.ad_status')]);
            }else{
                return  false;
            }
        }
    }

//广告的删除
    public static function addel($id){
        return self::destroy($id);
    }

//如果是GET的话返回一条用ID查到的数据
    public static function adbyid($id){
        //如果是GET的话返回一条用ID查到的数据
        return self::where('ad_id',$id)->find()->toArray();
    }

//如果是POST的话开始修改
    public  function adupdate($file,$id){
        //先查
        $data=$this->where('ad_id',$id)->find();
        //在修改
        //先调用公共区的文件上传
        //先判断文件是否为空
        if(empty($file)){
            $data->ad_start_time=strtotime(input('post.ad_start_time'));
            $data->ad_end_time=strtotime(input('post.ad_end_time'));
            $data->ad_status=input('post.ad_status');
            return $data->save();
        }else{
            $url=self::updatefile($file);
            $data->ad_img=$url;
            $data->ad_start_time=strtotime(input('post.ad_start_time'));
            $data->ad_end_time=strtotime(input('post.ad_end_time'));
            $data->ad_status=input('post.ad_status');
            return $data->save();
        }
    }


}

