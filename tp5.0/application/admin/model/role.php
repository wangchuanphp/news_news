<?php

namespace app\admin\model;

use think\Db;
use think\Model;

class role extends Model{
    protected $table = 'role';
    //添加角色信息并且将角色所有的权限插入到角色权限关系表中
    //需要传入添加的角色名称和要拥有的权限ID，需要传入两个数据
    public function add(){
        $role_name=request()->post('role_name');
        $control_id=request()->post('control_id/a');
        $this->role_name=$role_name;
        if($this->save()){
            $role_id=intval($this->role_id);
            $control=[];
            foreach($control_id as $key => $val){
                $control[]=['role_id'=>$role_id,'control_id'=>$val];
            }

            $controls=Db::table('role_control')->insertAll($control);

            return $controls;
        }else{
            return false;
        }
    }
    //删除角色ID并且根据角色ID将角色权限关系表中的该角色所有权限删除，需传入一个角色ID
    public function deleteRole(){
        $role_id = request()->post('role_id');
        Db::table('role_control')->where("role_id in ($role_id )")->delete();
        return self::destroy($role_id);
    }
    //需要传入添加的新的角色名称、角色ID和要拥有新的权限ID，需要传入三个数据
    public function updateRole(){
        $role_id=request()->post('role_id');
        $role_name=request()->post('role_name');
        $control_id=request()->post('control_id/a');
        $role = self::get($role_id);
        $role->role_name=$role_name;
        if(Db::table('role_control')->where("role_id in ($role_id)")->delete()){
            $control=[];
            foreach($control_id as $key => $val){
                $control[]=['role_id'=>$role_id,'control_id'=>$val];
            }

            $controls=Db::table('role_control')->insertAll($control);

            return $controls;
        }else{
            return false;
        }
    }
    //查看所有角色的信息及权限
    public function selectRole(){
        $roles=self::all()->toArray();
        foreach($roles as $key => $val){
            $control=Db::table('role_control')->field('control_id')->where('role_id',$val['role_id'])->select();
            foreach($control as $ke => $va){
                $control_name=Db::table('control')->field('control_name')->where('control_id',$va['control_id'])->select();
                foreach($control_name as $k => $v){
                    $control[$ke]['control_name']=$v['control_name'];
                }
            }
            $roles[$key]['control']=$control;
        }
        return $roles;
    }
    //根据角色ID查看该角色信息及权限
    public function selectRoleById($role_id){
        $roles=self::get($role_id)->toArray();
        $control=Db::table('role_control')->field('control_id')->where('role_id',$role_id)->select();
        $controls=[];
        foreach($control as $key => $val){
            $controls[]=$val['control_id'];
        }
        $roles['control']=$controls  ;
        return $roles;
    }
	//查询所有的角色
    public static function roleall(){
        return self::all()->toArray();
    }




}
