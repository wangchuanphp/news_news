<?php

namespace app\admin\model;
use think\db;
use think\Request;
use think\Model;
use Qiniu\Auth as Auth;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;
class News extends Model
{
    public function add(){
        if (empty(Request::instance()->file('news_img'))){
            $this->news_title=Request::instance()->post('news_title');
            $this->news_order=Request::instance()->post('news_order',0);
            $this->news_edit=Request::instance()->post('news_edit');
            $this->news_isposn=Request::instance()->post('news_isposn');
            $this->news_ishead=Request::instance()->post('news_ishead');
            $this->news_desc=Request::instance()->post('news_desc');
            $this->cate_id=Request::instance()->post('cate_id');
            $this->news_create_time=time();
            $this->save();
            $news_id=$this->news_id;
            $map=[
                'img_url'=>"",
                'img_time'=>time(),
                'news_id'=>$news_id
            ];
                $img=Db::table('img')->insert($map);
            return $img;
        }
        $this->news_title=Request::instance()->post('news_title');
        $this->news_order=Request::instance()->post('news_order',0);
        $this->news_edit=Request::instance()->post('news_edit');
        $this->news_isposn=Request::instance()->post('news_isposn');
        $this->news_ishead=Request::instance()->post('news_ishead');
        $this->news_desc=Request::instance()->post('news_desc');
        $this->cate_id=Request::instance()->post('cate_id');
        $this->news_create_time=time();
        $this->save();
        $news_id=$this->news_id;
        $news_img=$this->uploads();
        $map=[
            'img_url'=>$news_img,
            'img_time'=>time(),
            'news_id'=>$news_id
       ];
        if($news_id){
            $img=Db::table('img')->insert($map);
        }
        return $img;
    }

    //七牛图片上传 存到数据库的路径不显示 用的本地上传
    public function uploads(){
        if(request()->isPost()){
            $file = request()->file('news_img');
            // 要上传图片的本地路径
            $filePath = $file->getRealPath();
            $ext = pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);  //后缀
            // 上传到七牛后保存的文件名
            $key =substr(md5($file->getRealPath()) , 0, 5). date('YmdHis') . rand(0, 9999) . '.' . $ext;
            require_once APP_PATH . '/../vendor/Qiniu/autoload.php';
            // 需要填写你的 Access Key 和 Secret Key
            $accessKey = config('ACCESSKEY');
            $secretKey = config('SECRETKEY');
            // 构建鉴权对象
            $auth = new Auth($accessKey, $secretKey);
            // 要上传的空间
            $bucket = config('BUCKET');
            $domain = config('DOMAINImage');
            $token = $auth->uploadToken($bucket);
            // 初始化 UploadManager 对象并进行文件的上传
            $uploadMgr = new UploadManager();
            // 调用 UploadManager 的 putFile 方法进行文件的上传
            list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);
            if ($err !== null) {
                return null;
            }else {
                //返回图片的完整URL
                return "pbmrl8wye.bkt.clouddn.com".'/'. $ret['key'];
            }

        }
    }
    //新闻修改
    public function upnews($news_id){
        if(request()->isPost()){

            //$newsModel=Db::table('news')->where('news_id',$news_id)->find();
            $newsModel= self::find($news_id);
            //return $newsModel;
        if (empty(request()->file('news_img'))){
            $newsModel->news_title=request()->post('news_title');
            $newsModel->news_order=Request::instance()->post('news_order',0);
            $newsModel->news_edit=Request::instance()->post('news_edit');
            $newsModel->news_isposn=Request::instance()->post('news_isposn');
            $newsModel->news_ishead=Request::instance()->post('news_ishead');
            $newsModel->news_desc=Request::instance()->post('news_desc');
            $newsModel->cate_id=Request::instance()->post('cate_id');
            $newsModel->news_create_time=time();
            return  $newsModel->save();
        }
            $newsModel->news_title=request()->post('news_title');
        $newsModel->news_order=Request::instance()->post('news_order',0);
        $newsModel->news_edit=Request::instance()->post('news_edit');
        $newsModel->news_isposn=Request::instance()->post('news_isposn');
        $newsModel->news_ishead=Request::instance()->post('news_ishead');
        $newsModel->news_desc=Request::instance()->post('news_desc');
        $newsModel->cate_id=Request::instance()->post('cate_id');
        $newsModel->news_create_time=time();
        $newsModel->save();
        $news_img=$this->uploads();
        $sss=Db::table('img')->where('news_id',$news_id)->update(['img_url'=>$news_img,'img_time'=>time()]);

        return $sss;
    }
}
}