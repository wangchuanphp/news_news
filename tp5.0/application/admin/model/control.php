<?php

namespace app\admin\model;

use think\Model;

class control extends Model{
    protected $table = 'control';
    //添加权限
    public function add(){
        $control_name=request()->post('control_name');
        $control_content=request()->post('control_content');
        $this->control_name=$control_name;
        $this->control_content=$control_content;
        return $this->save();
    }
    //删除权限
    public function deleteControl(){
        $control_id=request()->post('control_id');
         return self::destroy($control_id);
    }
    //展示所有权限
    public function showControl(){
        $control=self::all()->toArray();
        $controls=[];
       if($control){
           return $control;
       }else{
           return $controls;
       }
    }







}
