<?php

namespace app\admin\model;

use think\Model;
use think\Db;
class admins extends Model
{

    // 设置当前模型对应的完整数据表名称
       protected $table = 'admin';

    //登录时验证使用
    public static function admin($name){
       return self::where('admin_name',$name)->find();
    }

    //管理员的修改
    //首先通过管理员的ID去查询某个管理员
    public static function adminbyid($admin_id){
      return  self::where('admin_id',$admin_id)->find()->toArray();
    }
    //通过用户的ID和角色的ID去查是否有
    public static function adminrolebyid($admin_id,$role_id,$admin_status){
        $admin_role=Db::table('admin_role')->where('admin_id',$admin_id)->where('role_id',$role_id)->find();
        self::where('admin_id',$admin_id)->update(['admin_status'=>$admin_status]);

        if($admin_role==null){
          return  Db::table('admin_role')->where('admin_id',$admin_id)->update(['role_id'=>$role_id]);
       }
        return true;
    }


    //管理员添加
    public static function adminadd(){
      $admin= self::create(['admin_name'=>input('post.admin_name'),'admin_pwd'=>md5(input('post.admin_pwd')),'admin_status'=>input('post.admin_status')]);
        return $admin->admin_id;
    }

    //管路员展示
    public static function adminall(){
        $admin=self::all()->toArray();
        foreach($admin as $k=>$v){
           $role = Db::table('admin_role')->where('admin_id',$v['admin_id'])->join('role','role.role_id=admin_role.role_id')->select();
            foreach($role as $key=>$val){
                $admin[$k]=array_merge($v,$val);
            }
        }
    return $admin;

    }
    #管理员的删除
    public static function admindel($admin_id){
        return self::where('admin_id',$admin_id)->delete();
    }



}
