<?php
namespace app\admin\controller;

use think\Controller;
use think\Validate;
use think\Db;
use think\Request;
use think\Session;

class Login extends Controller{
    public function login(){
        if(request()->isGet()){
            return view('login');
        }
        if(request()->isPost()){
            $admin_name=request()->post('admin_name','');
            $admin_pwd =request()->post('admin_pwd', '');
            //验证用户名及密码
            $validate = new Validate([
                'admin_name'  => 'require|max:25',
                'admin_pwd' => 'require|max:25'
            ]);

            $data=[
                'admin_name'=>$admin_name,
                'admin_pwd' =>$admin_pwd
            ];

            if (!$validate->check($data)) {
                return '用户名或账号错误';
            }

            $code=request()->post('code','');
            if(!captcha_check($code)){
                //验证失败
                $this->error('验证码错误');
            };
            $admin=Db::table('admin')
                ->where('admin_name',$admin_name)
                ->where('admin_pwd',$admin_pwd)
                ->find();
            if($admin){
                //验证成功,存session
                Session::set('admin',$admin);
                //var_dump(Session::get('admin'));exit;
                //$this->assign('admin',$admin);
                //return $this->fetch('admin');
                $this->success('登录成功','admin/Index/index');
            }else{
                //验证失败
                $this->error('账号或密码错误');
            }
        }
    }
    //退出
    public function logout(){
        Session::delete('admin');
        $this->success('退出成功','admin/Login/login');
    }
}