<?php

namespace app\admin\controller;

use app\admin\controller\Common;
use think\Controller;
use think\Db;
use think\Request;
use think\Validate;

class Comment extends Common{

    //评论添加(前台传输过来数据)
    public function add($comment_content,$user_id,$news_id){
        //验证用户填写的评论内容
//        $validate = new Validate([
//            'comment_content'  => '/^习近平|毛泽东|暴力|黄色$/',
//        ]);
//        $data=[
//            'comment_content'=>$comment_content,
//        ];
//        if ($validate->check($data)) {
//            return json(['status'=>0,'msg'=>'评论内容不合法']);
//        }
        $data=[
            'comment_content'=>$comment_content,
            'user_id'=>$user_id,
            'comment_time'=>time(),
            'news_id'=>$news_id,
        ];
        $comment_id=Db::table('comment')->insertGetId($data);
        if($comment_id){
            //查询刚插入的评论，返回给前台
            $comment=Db::table('comment')->where('comment_id',$comment_id)->find();
            return json(['status'=>1,'msg'=>'添加成功','data'=>$comment]);
        }else{
            return json(['status'=>0,'msg'=>'添加失败']);
        }
    }

    //后台评论展示
    public function index(){
        //获取所有的评论
        $comment=Db::table('comment')->order('comment_time asc')->select();
        foreach($comment as $k=>$v){
            //获取评论所属用户
            $comment[$k]['user_id']=Db::table('user')->where('user_id',$v['user_id'])->find();
            //获取评论所属新闻
            $comment[$k]['news_id']=Db::table('news')->where('news_id',$v['news_id'])->find();
        }
        //获取评论总条数
        $comments=count($comment);
        $this->assign('comment',$comment);
        $this->assign('comments',$comments);
        return view('index');
    }

    //后台评论删除
    public function del($comment_id){
        //对于一些不法评论的删除
        if(Db::table('comment')->where('comment_id',$comment_id)->delete()){
            return redirect('admin/Comment/index');
        }else{
            return back();
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
