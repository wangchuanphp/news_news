<?php
namespace app\admin\controller;

use think\Controller;
use think\Validate;
use think\Db;
use think\Request;
use think\Session;

class Common extends Controller{
    public function __construct(){
        //防非法登录
        parent::__construct();
        if(!Session::has('admin')){
            $this->success('非法登录','admin/Login/login');
        }
    }
}