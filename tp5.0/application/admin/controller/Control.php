<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Control extends Common{
    //权限插入添加
    public function addControl(){
        if(request()->isGet()){
            return $this->fetch();
        }
        if(request()->isPost()){
            $control = new \app\admin\model\control();
            $control=$control->add();
            header('content-type:application:json;charset=utf8');
            header('Access-Control-Allow-Origin:*');
            header('Access-Control-Allow-Methods:POST');
            header('Access-Control-Allow-Headers:x-requested-with,content-type');
            if($control){
                return redirect('showControl');
            }else{
                return back();
            }
        }
    }
    //权限删除
    public function delControl(){
        $control = new \app\admin\model\control();
        $control=$control->deleteControl();
        header('content-type:application:json;charset=utf8');
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:POST');
        header('Access-Control-Allow-Headers:x-requested-with,content-type');
        if($control){
            return ["status"=>1,"msg"=>"删除权限成功"];
        }else{
            return ["status"=>0,"msg"=>"删除权限失败"];
        }
    }
    //查询所有权限
    public function showControl(){
        $control = new \app\admin\model\control();
        $control=$control->showControl();

            $count=count($control);
            $this->assign('count',$count);
            $this->assign('control',$control);
            return $this->fetch();

    }



}
