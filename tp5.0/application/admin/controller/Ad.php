<?php

namespace app\admin\controller;
vendor('qiniu.autoload');
use app\admin\model\ads;
use think\Controller;
use think\Request;

class Ad extends Common
{
    #广告展示
    public function index()
    {
        $data=ads::adall();
        $this->assign('data',$data);
        return $this->fetch();

    }

    #广告的添加
    public function adadd()
    {
        if($this->request->isGet()){
            return $this->fetch();
        }
        if($this->request->isPost()){
            $file=request()->file('ad_img');
            if(ads::adadd($file)){
                $this->redirect('index');
            }else{
                $this->redirect('index');
            }
        }
    }

    #广告删除
    public  function addel($ad_id){
        if(ads::addel($ad_id)){
            $this->redirect('index');
        }else{
            $this->redirect('index');
        }
    }
    #广告的修改
    public function update($ad_id){
        if($this->request->isGet()){
            $data=ads::adbyid($ad_id);
            $this->assign('data',$data);
            return $this->fetch();
        }
        if($this->request->isPost()){
            $file=request()->file('ad_img');
            $ad=new ads();
            if($ad->adupdate($file,$ad_id)){
                $this->redirect('index');
            }else{
                $this->redirect('index');
            }
        }
    }
}
