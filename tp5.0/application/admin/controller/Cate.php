<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;

class Cate extends Common
{
    /**
     * 新闻分类展示
     *
     * @return \think\Response
     */
    public function index(){
        $cates=Db::table('cate')->select();
        $count=count($cates);
        $this->assign('count',$count);
        $this->assign('cates',$cates);
        return $this->fetch();
        //接口
//        header('content-type:application:json;charset=utf8');
//        header('Access-Control-Allow-Origin:*');
//        header('Access-Control-Allow-Methods:POST');
//        header('Access-Control-Allow-Headers:x-requested-with,content-type');
//        return json(['cates'=>$cates]);

    }

    /**
     * 新闻分类添加
     *
     * @return \think\Response
     */
    public function add(){
        if(request()->isGet()){
            return $this->fetch();
        }
        if(request()->isPost()){
            $data['cate_name']=request()->post('cate_name');
             $data['cate_desc']=request()->post('cate_desc');
            $data['cate_order']=request()->post('cate_order');
             $data['cate_isshow']=request()->post('cate_isshow');
            $data['cate_time']=time();
            $cates=Db::table('cate')->insert($data);
            if($cates){
 //               return json(['status'=>0,'msg'=>'添加分类成功']);
                $this->success("添加分类成功",'admin/Cate/index');
            }else{
 //               return json(['status'=>1,'msg'=>'添加失败']);
                $this->error("添加分类失败");
            }
        }
    }
    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $cate_id)
    {
        if($request->isGet()){
            $cates=Db::table('cate')->where('cate_id',$cate_id)->find();
//            return json(['cates'=>$cates]);
            $this->assign('cates',$cates);
            return $this->fetch();
        }
        if($request->isPost()){
            $data['cate_name']=request()->post('cate_name');
            $data['cate_desc']=request()->post('cate_desc');
            $data['cate_order']=request()->post('cate_order');
            $data['cate_isshow']=request()->post('cate_isshow');
            $data['cate_time']=time();
            $cates=Db::table('cate')->where('cate_id',$cate_id)->update($data);
            if($cates){
//                return json(['status'=>0,'msg'=>'修改分类成功']);
                $this->success('修改成功','admin/Cate/index');
            }else{
//                return json(['statua'=>1,'msg'=>'修改失败']);
                $this->error("修改失败");
            }
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delcate($cate_id)
    {
        $news=Db::table('news')->where('cate_id',$cate_id)->select();
        //halt($news);
        if($news){
            $this->error("次分类下有新闻不能删除");
//            return json(['status'=>0,'msg'=>'次分类下有新闻不能删除']);
        }else{
            $cate=Db::table('cate')->where('cate_id',$cate_id)->delete();
            if($cate){
//                return json(['status'=>0,'msg'=>'删除分类成功']);
                $this->success("删除成功",'admin/Cate/index');
            }else{
//                return json(['status'=>1,'msg'=>'删除失败']);
                $this->error("删除失败");
            }
        }
    }
}
