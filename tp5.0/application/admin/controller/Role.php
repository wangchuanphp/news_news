<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Role extends Common{

    //角色添加
    public function addRole(){
        if(request()->isGet()){
            $role=new \app\admin\model\control();
            $roles=$role->showControl();
            $this->assign('roles',$roles);
          return  $this->fetch();
        }
        if(request()->isPost()){
            $role = new \app\admin\model\role();
            $roles=$role->add();
//            var_dump($roles);exit;
            if($roles){
                return redirect('showRole');
            }else{
                return 1;
            }
        }
    }
    //角色删除
    public function delRole(){
        $role = new \app\admin\model\role();
        $roles=$role->deleteRole();
        if($roles){
            return ["status"=>1,"msg"=>"删除角色成功"];
        }else{
            return ["status"=>0,"msg"=>"删除角色失败"];
        }
    }
    //角色编辑
    public function upRole($role_id){
        if(request()->isGet()){
            $roless = new \app\admin\model\role();
            $role=new \app\admin\model\control();
            $roles=$role->showControl();
            $rol=$roless->selectRoleById($role_id);
            $this->assign('roles',$roles);//展示所有权限
            $this->assign('rol',$rol);//角色信息
           return $this->fetch();
        }
        if(request()->isPost()){
            $role = new \app\admin\model\role();
            $roles=$role->updateRole();
            if($roles){
                return redirect('showRole');
            }else{
                return false;
            }
        }
    }
    //角色查看
    public function showRole(){
        $role = new \app\admin\model\role();
        $roles=$role->selectRole();
        $tiao=count($roles);
        $this->assign('roles',$roles);
        $this->assign('tiao',$tiao);
        return $this->fetch();


    }
}
