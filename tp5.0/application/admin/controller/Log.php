<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;

class Log extends Common{
    public function index(){
        $logs=Db::table('log')->select();
        $this->assign("logs",$logs);
        return $this->fetch();
    }

}
