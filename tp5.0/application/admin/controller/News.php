<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;


use Qiniu\Auth as Auth;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;
class News extends Common{
//新闻添加
    public function add()
    {
        if (request()->isGet()) {
            $cates = Db::table('cate')->field('cate_id,cate_name')->select();
//            return json(['cates'=>$cates]);
            $this->assign('cates', $cates);
            return $this->fetch();
        }
        if (request()->isPost()) {

            $newsModel = new \app\admin\model\news();
            $img = $newsModel->add();
            if ($img) {
                $this->success('添加成功','admin/News/index');
//                return json(['status' => 0, 'msg' => '添加成功']);
            } else {
                $this->error('添加失败');
//                return json(['status' => 1, 'msg' => '添加失败']);
            }
        }
    }
//新闻展示
    public function index(){
        $news=Db::table('news')->join('cate','news.cate_id=cate.cate_id')->select();
//        $img=Db::table('img')->join('news','news.news_id=img.news_id')->select();
        //halt($img);
        $count=count($news);
        $this->assign('count',$count);
        $this->assign('news',$news);
        return $this->fetch();
//        return json(['大曼'=>1,'小曼'=>0]);
    }
    public function imgs(){
        if(request()->isPost()){
            $news_id=input('post.news_id');
            $img=Db::table('img')->where('news_id',$news_id)->join('news','news.news_id=img.news_id')->find();
            halt($img);
        }

    }

//图片上传
    public function upload(){
        $news_img=request()->file('news_img');
        if(empty($news_img)){
            $this->error('请上传图片');
        }
        $info=$news_img->move('static/uploads');
        if($info){
           $tt= date("Ymd");
            $this->success('文件上传成功');
           return "__STATIC__/uploads/$tt/".$info->getFilename();
        }else{
            $this->error($news_img->getError());
        }
    }
//新闻修改
    public function update($news_id){
        if(request()->isGet()){
            $news=Db::table('news')->where('news_id',$news_id)->find();
            $img=Db::table('img')->where('news_id',$news_id)->select();

            foreach($img as $key=> $val){
                $img=$val;
            }
           // halt($img);
            $cates = Db::table('cate')->field('cate_id,cate_name')->select();
            //halt($img);
            $this->assign('img',$img);
            $this->assign('cates',$cates);
            $this->assign('news',$news);
            return $this->fetch();
//           return json(['news'=>$news,'cates'=>$cates,'img'=>$img]);
        }
        if(request()->isPost()){
            $newsModel = new \app\admin\model\News();
            $img=$newsModel->upnews($news_id);
            //halt($img);
            if($img){
                $this->success('修改成功','admin/News/index');
//                return json(['status' => 0, 'msg' => '修改成功']);
            }else{
                $this->error('修改失败');
//                return json(['status' => 1, 'msg' => '修改失败']);
            }
        }
    }
//新闻删除
    public function delnews($news_id)
    {

//            $news_id = request()->post('news_id');
//        $news_imgs=Db::table('news')->join('img','news.news_id=img.news_id')->where('news_id',$news_id)->select();
            $news = Db::table('news')->where('news_id', $news_id)->find();
            $imgs = Db::table('img')->where('news_id', $news_id)->select();
            if ($news && !$imgs) {
                $news = Db::table('news')->where('news_id', $news_id)->delete();

                if ($news) {
                    $img = Db::table('img')->where('news_id', $news_id)->delete();
                    if ($img) {
                        $this->success('删除成功','admin/News/index');
//                        return json(['status' => 0, 'msg' => '删除新闻成功']);
                    }
                    $this->success('删除成功','admin/News/index');
//                    return json(['status' => 0, 'msg' => '删除成功']);
                } else {
                    $this->serror('删除失败');
//                    return json(['status' => 1, 'msg' => '删除新闻失败']);
                }
            }
        }

    //七牛图片上传 存到数据库的路径不显示 用的本地上传
    public function uploads(){
        if(request()->isPost()){
            $file = request()->file('news_img');
            // 要上传图片的本地路径
            $filePath = $file->getRealPath();
            $ext = pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);  //后缀
            // 上传到七牛后保存的文件名
            $key =substr(md5($file->getRealPath()) , 0, 5). date('YmdHis') . rand(0, 9999) . '.' . $ext;
            require_once APP_PATH . '/../vendor/Qiniu/autoload.php';
            // 需要填写你的 Access Key 和 Secret Key
            $accessKey = config('ACCESSKEY');
            $secretKey = config('SECRETKEY');
            // 构建鉴权对象
            $auth = new Auth($accessKey, $secretKey);
            // 要上传的空间
            $bucket = config('BUCKET');
            $domain = config('DOMAINImage');
            $token = $auth->uploadToken($bucket);
            // 初始化 UploadManager 对象并进行文件的上传
            $uploadMgr = new UploadManager();
            // 调用 UploadManager 的 putFile 方法进行文件的上传
            list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);
            if ($err !== null) {
                return null;
            }else {
                //返回图片的完整URL
                return "pbmrl8wye.bkt.clouddn.com".'/'. $ret['key'];
            }
        }
    }













    public function aadd(){
//                    if (empty($_FILES['news_img']['tmp_name'])){
//                $data['news_title']=Request::instance()->post('news_title');
//                $data['news_order']=Request::instance()->post('news_order',0);
//                $data['news_edit']=Request::instance()->post('news_edit');
//                $data['news_isposn']=Request::instance()->post('news_isposn');
//                $data['news_ishead']=Request::instance()->post('news_ishead');
//                $data['news_desc']=Request::instance()->post('news_desc');
//                $data['cate_id']=Request::instance()->post('cate_id');
//                $data['news_create_time']=time();
//                $newss=Db::table('news')->insert($data);
//                if($newss){
//                    return json(['status'=>0,'msg'=>'添加成功']);
////                $this->success("添加成功",'admin/news/index');
//                }else{
////                $this->error("添加失败");
//                    return json(['status'=>1,'msg'=>'添加失败']);
//                }
//            }
//            $data['news_title']=Request::instance()->post('news_title');
//            $data['news_order']=Request::instance()->post('news_order',0);
//            $data['news_edit']=Request::instance()->post('news_edit');
//            $data['news_isposn']=Request::instance()->post('news_isposn');
//            $data['news_ishead']=Request::instance()->post('news_ishead');
//            $data['news_desc']=Request::instance()->post('news_desc');
//            $data['cate_id']=Request::instance()->post('cate_id');
//            $data['news_create_time']=time();
//            $news_img=$this->uploads();
//            $news_id=Db::table('news')->insertGetId($data);
//            $map=[
//                'img_url'=>$news_img,
//                'img_time'=>time(),
//                'news_id'=>$news_id
//            ];
//            if($news_id){
//                $img=Db::table('img')->insert($map);
//                if($img){
//                    return json(['status'=>0,'msg'=>'添加成功']);
////                $this->success("添加成功",'admin/news/index');
//                }else{
////                $this->error("添加失败");
//                    return json(['status'=>1,'msg'=>'添加失败']);
//                }
//            }else{
//                return json(['status'=>1,'msg'=>'添加失败']);
//            }
    }


}
