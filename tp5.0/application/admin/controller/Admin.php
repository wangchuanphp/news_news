<?php

namespace app\admin\controller;

use app\admin\model\admins;
use app\admin\model\role;
use think\Controller;
use think\Request;
use think\Db;

class Admin extends Common
{
    #管理员首页
    public function index()
    {
    $admin=admins::adminall();
        $this->assign('admin',$admin);
//       print_r($admin);exit;
        return $this->fetch();
    }

    #管理员的添加
    public function adminadd(Request $request)
    {
        //
        if($this->request->isGet()){
            $role=role::roleall();
            $this->assign('role',$role);
            return $this->fetch();
        }
        if($this->request->isPost()){
            $admin_id=admins::adminadd();
            $role_id=input("post.adminRole");
            $data=['admin_id'=>$admin_id,'role_id'=>$role_id];
            if($data){
                Db::table('admin_role')->insert($data);
                return redirect('index');
            }else{
                return redirect('index');
            }
        }
    }

    #管理员的修改
    public function adminupdate($admin_id)
    {
        //
       if($this->request->isGet()){
           //查出某个管理员的信息
           $admin=admins::adminbyid($admin_id);
           //查询所有的角色
           $roles=role::roleall();
           //然后去查该管理员的角色
            $role=Db::table('admin_role')->where('admin_id',$admin_id)->join('role','role.role_id=admin_role.role_id')->select();
           $this->assign('role',$role);
            $this->assign('admin',$admin);
            $this->assign('roles',$roles);
            return $this->fetch();
       }
        if($this->request->isPost()){
            //先去查询
            $admin_id=input('post.admin_id');
            $role_id=input('post.role_id');
            $admin_status=input('post.admin_status');
                if(admins::adminrolebyid($admin_id,$role_id,$admin_status)){
                    return redirect('index');
                }else{
                    return redirect('index');
                }
        }
    }

    #管理员的删除
    public function admindel($admin_id)
    {
        if(admins::admindel($admin_id)){
            return redirect('index');
        }
    }
}
