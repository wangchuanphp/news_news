
-- -----------------------------
-- Table structure for `user`
-- -----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户表',
  `user_name` varchar(100) NOT NULL COMMENT '用户帐号',
  `user_pwd` varchar(200) NOT NULL COMMENT '用户密码',
  `user_face` varchar(200) DEFAULT NULL COMMENT '用户头像',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `user`
-- -----------------------------
INSERT INTO `user` VALUES ('1', 'xiaowang', '123123', 'http://p92yyw5d5.bkt.clouddn.com/2018-05-25_5b07b842c8724.jpg');
INSERT INTO `user` VALUES ('2', '方法', '111', 'http://p92yyw5d5.bkt.clouddn.com/2018-05-25_5b07b842c8724.jpg');
INSERT INTO `user` VALUES ('3', '哈哈', '123456', 'http://p92yyw5d5.bkt.clouddn.com/2018-05-25_5b07b842c8724.jpg');
INSERT INTO `user` VALUES ('4', 'hhe', '123', '');
INSERT INTO `user` VALUES ('5', '123', '123', '');
INSERT INTO `user` VALUES ('6', '234', '234', '');
INSERT INTO `user` VALUES ('7', '11', '11', '');
INSERT INTO `user` VALUES ('8', '11111', '111', '');
INSERT INTO `user` VALUES ('9', '小红形式得', '123dvd', '');
INSERT INTO `user` VALUES ('10', '小红', '123', '');
INSERT INTO `user` VALUES ('11', '小红', '123', '');
INSERT INTO `user` VALUES ('12', '哈哈 顶顶顶', '11111', '');
INSERT INTO `user` VALUES ('13', 'xiaowang', '123123', '');
INSERT INTO `user` VALUES ('14', 'women', '111111', '');
INSERT INTO `user` VALUES ('15', 'women', '111111', '');
INSERT INTO `user` VALUES ('16', 'womena', '111111', '');
INSERT INTO `user` VALUES ('17', 'womena1', '111111', '');
